fn main() { println!("Hello, world!"); }

#[cfg(test)] mod basic_tests {

    use rstest::rstest;

    #[rstest]
    fn passing_assert() {
        assert!(true);
    }

    #[rstest]
    #[should_panic(expected = "assertion failed: false")]
    fn failing_assert() {
        assert!(false);
    }

    #[rstest(a, b)]
    #[case(0, 0)]
    #[case(1, 1)]
    #[should_panic] #[case(0, 1)]
    #[should_panic(expected = "assertion failed: `(left == right)`")] #[case(0, 1)]
    fn equals_test(a: usize, b: usize) {
        assert_eq!(a, b);
    }

    #[rstest]
    fn sum_test(
        #[values(1, 2, 3)] a: i32,
        #[values(4, 5, 6)] b: i32
    ) {
        assert!(a + b > 0);
    }
}

fn fib(value: i32) -> i32 {
    match value {
        0 => 0,
        1 | 2 => 1,
        _ => fib(value - 1) + fib(value - 2),
    }
}

#[cfg(test)] mod fibonacci_tests {

    use rstest::rstest;
    use super::fib;

    #[rstest(
    value, expected,
    case(0, 0), case(1, 1), case(2, 1),
    case(3, 2), case(4, 3), case(5, 5),
    case(6, 8), case(7, 13), case(8, 21),
    #[should_panic] case(3, 3),
    )]
    fn fib_test(value: i32, expected: i32) {
        assert_eq!(fib(value), expected);
    }
}

struct TestData {
    pub initial: String,
    pub test_list: Vec<usize>,
}

impl TestData {

    fn new() -> TestData {
        TestData {
            initial: "initial string".to_string(),
            test_list: vec![],
        }
    }

    fn add_item(&mut self, value: usize) {
        self.test_list.push(value);
    }
}

#[cfg(test)] mod test_data_tests {

    use rstest::rstest;
    use super::TestData;

    #[rstest]
    fn test_data_test() {
        let mut test_data = TestData::new();
        assert_eq!(test_data.initial, "initial string".to_string());
        assert!(test_data.test_list.is_empty());
        test_data.add_item(0);
        test_data.add_item(1);
        assert_eq!(test_data.test_list.len(), 2);
    }

    use rstest::fixture;

    #[fixture(items = vec![1, 2, 3])]
    fn test_data_item(items: Vec<usize>) -> TestData {
        let mut item = TestData::new();
        for i in items { item.add_item(i) };
        item
    }

    #[rstest]
    fn test_data_fixture_test(test_data_item: TestData) {
        assert_eq!(test_data_item.test_list.len(), 3);
    }

    #[rstest]
    fn test_data_fixture_ext_test(mut test_data_item: TestData) {
        test_data_item.add_item(4);
        assert_eq!(test_data_item.test_list.len(), 4);
    }
}
